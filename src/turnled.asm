;   TurnLED - Turning a LED
;   Copyright (C) 2009  Steven Rodriguez
;
;   This program is free software: you can redistribute it and/or modify
;   it under the terms of the GNU General Public License as published by
;   the Free Software Foundation, either version 3 of the License, or
;   (at your option) any later version.
;
;   This program is distributed in the hope that it will be useful,
;   but WITHOUT ANY WARRANTY; without even the implied warranty of
;   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;   GNU General Public License for more details.
;
;   You should have received a copy of the GNU General Public License
;   along with this program.  If not, see <http://www.gnu.org/licenses/>.
;
;   If this program has problems please contact me at:
;
;   stevencrc@digitecnology.zapto.org

;==============================================
;TurnLED (2009 - Steven Rodriguez)
;==============================================

;Assembler directives
;=============================
	list p=16f628a
	include p16f628a.inc
	__config b'11111100001001'
	org 0x00
	goto Start

;Program
;=============================
Start
	goto Initialize
Initialize
	bcf STATUS,RP1 ; Go to bank 0
	bcf STATUS,RP0
	clrf PORTA ; Clear PORTA register
	clrf PORTB ; Clear PORTB register
	bcf STATUS,RP1 ; Go to bank 1
        bsf STATUS,RP0
	bcf TRISA,0 ; Make RA0 output (0 = output, 1 = input) 
        bsf TRISB,0 ; Make RB0 input
	bcf STATUS,RP1 ; Go to bank 0
        bcf STATUS,RP0	
	goto Cycle
Cycle
	btfss PORTB,0 ; Check if switch is on
	bsf PORTA,0 ; Turn on the led
	bcf PORTA,0 ; Turn off the led
	goto Cycle
	end
